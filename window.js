class WebWindow extends HTMLElement {
    width = 300;
    height = 200;

    static observedAttributes = ["width", "height"];

    constructor() {
        super();
    }

    connectedCallback() {
        const shadow = this.attachShadow({ mode: "open" });
        const style = document.createElement("style");
        style.textContent = `
            .web-window {
                position: absolute;
                border: 2px solid blue;
                background-color: blue;
                
                width: ${+this.width}px;
                height: ${+this.height + 20}px;
                display: flex;
                flex-direction: column;
                justify-content: flex-end;
            }`
        shadow.appendChild(style);

        const borderDiv = document.createElement("div");
        borderDiv.classList.add("web-window");

        shadow.appendChild(borderDiv);
        dragElement(borderDiv);

        const innerDiv = document.createElement("div");
        borderDiv.appendChild(innerDiv);

        const canvas = document.createElement("canvas");
        canvas.style.background = "black";
        const ctx = canvas.getContext("2d");
        canvas.width = this.width;
        canvas.height = this.height;
        ctx.fillStyle = "orange";
        ctx.lineWidth = 3;
        innerDiv.appendChild(canvas);

        const arc = {x: 30, y: 30, radius: 20, movingUp: true, movingRight: true};

        const moveArc = () => {
            if (arc.movingUp) {
                if (arc.y - arc.radius> 0) {arc.y-=4;}
                else {arc.movingUp = false;}
            } 
            else {
                if (arc.y + arc.radius < canvas.height) {arc.y+=4;}
                else {arc.movingUp = true;}
            }

            if (arc.movingRight) {
                if (arc.x + arc.radius < canvas.width) {arc.x+=4;}
                else {arc.movingRight = false;}
            } 
            else {
                if (arc.x - arc.radius > 0) {arc.x-=4;}
                else {arc.movingRight = true;}
            }
        }

        const draw = () => {
            moveArc();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.beginPath();
            ctx.arc(
                arc.x,
                arc.y,
                arc.radius,
                0, 
                Math.PI * 2
            );
            ctx.fill();
            window.requestAnimationFrame(draw);
        }
        draw();
    }

    attributeChangedCallback(property, oldValue, newValue) {
        if (oldValue === newValue) return;
        this[property] = newValue;
    }
}

customElements.define("web-window", WebWindow);

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

